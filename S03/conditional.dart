

void main() {
  print(showServiceItems('robotics'));
  print(determineTyphoonIntensity(85));
}

List <String> showServiceItems(String category){
  if( category == 'apps'){
      return ['native','android','ios','web'];
  } else if (category == 'cloud'){
    return ['azure','microservices'];
  } else if (category == 'robotics'){
    return ['sensors', 'fleet-tracking','real time communication'];
  } else {
    return [];
  }
}


String determineTyphoonIntensity(int windSpeed){

    if(windSpeed < 30){
      return 'Not a typhoon yet';
    } else if (windSpeed <= 61){
      return 'Trophical depression detected';
    } else if (windSpeed >= 61 && windSpeed <= 88){
      return 'Tropical storm detected';
    } else if (windSpeed >= 89 && windSpeed <= 117){
      return 'Severe tropical storm detected';
    } else {
      return 'Typhoon detected';
    }
}

