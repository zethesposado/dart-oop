
void main(){

Person person =new Person(firstName: 'John', lastName: 'Smith');
Employee employee =  new Employee(lastName: 'Doe', firstName: 'Jane', employeeID: 'ID-001');

  print(person.getFullName());
  print(employee.getFullName());

}
class Person{
  String firstName;
  String lastName;


  Person({
    required  this.firstName,
    required  this.lastName
  });
  String getFullName(){
    return '${this.firstName} ${this.lastName}';
  }
}
class Employee extends Person{
  //in spirit, the firstName and lastName is inherited by Employee from Person
  //The Employee also inherits the getFullName from Person
  //inheritance then allows us to write less code
  String employeeID;
  
  Employee({
    required String firstName, 
    required String lastName,
    required this.employeeID
    }) : super(
      firstName: firstName, 
      lastName: lastName
    );
}