



void main(List<String> args) {
      Equipment equipment = new Equipment();
      equipment.name = 'Equipment-001';
      print(equipment.name);

      Loader loader = new Loader();
      loader.name = 'Loader-001';
      print(loader.name);
      print(loader.getcategory());
     
      WheelLoader wheelLoader = new WheelLoader();
      wheelLoader.name = 'Wheel Loader-001';
      print(wheelLoader.name);
      print(wheelLoader.getcategory());

  
}

class Equipment {
      String? name;
}
class Loader extends Equipment {
      String getcategory(){
          return '${this.name} is a loader';
      }
}

class WheelLoader extends Loader {
      String getcategory(){

          print(super.getcategory());
          return '${this.name} is a wheel loader';
      }
}