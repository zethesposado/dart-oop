

void main(List<String> args) {
      Equipment equipment = new Equipment();
      equipment.name = 'Equipment-001';
      print(equipment.name);

      Loader loader = new Loader();
      loader.name = 'Loader-001';
      print(loader.name);
      print(loader.getcategory());
     
      Crane crane = new Crane();
      crane.name = 'Crane -001';
      print(crane.name);
      print(crane.getcategory());
}

class Equipment {
      String? name;
}

class Loader extends Equipment {
      String getcategory(){
          return '${this.name} is a loader';
      }
}

class Crane extends Equipment {
      String getcategory(){

          return '${this.name} is a crane';
      }
}