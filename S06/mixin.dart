

void main(List<String> args) {
      Equipment equipment = new Equipment();
      equipment.name = 'Equipment-001';
      print(equipment.name);

      Loader loader = new Loader();
      loader.name = 'Loader-001';
      print(loader.name);
      print(loader.getcategory());
      loader.moveForward(30);
      print(loader.acceleration); 
     
      Car car = new Car();
      car.name = 'Car-001';
      print(car.name);
      print(car.getcategory());
      car.moveForward(30);

}

class Equipment {
      String? name;
}

class Loader extends Equipment with Movement {
      String getcategory(){
          return '${this.name} is a loader';
      }
}
class Car  with Movement {

      String? name;
      String getcategory(){

          return '${this.name} is a car';
      }
}

mixin Movement{
    num? acceleration;

    void moveForward(num acceleration){
      this.acceleration = acceleration;
      print("The vehicle is moving foward");
    }  
}