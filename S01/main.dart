// The keyword 'void' means, the function will not return anything
//THe syntax of a function is: return-type function-name (parameters) {}
void main(){
  //console.log
  //System.out.println()

    String firstName = 'John';
    String? middleName = null; //"?" to accept null values
    String lastName = 'Smith';
    int age = 31; //numbers w/o decimal points
    double height = 172.45; // for numbers w/ decimal points
    num weight = 75; // can accepts numbers w/ or w/o decimal points
   // name = number.toString(); //can no longer change data type

    bool isRegistered = false;
    List<num> grades = [98.2, 89, 87.88, 91.2]; // List = array
    //Map person = new Map() 
    // Map is an object, one method is to create new Map
    Map<String, dynamic> personA = {
        'name': 'Brandon',
        'batch': 213, 
    };

    //Alternative syntax for declaring an object
    Map <String, dynamic> personB = new Map();
        personB['name'] = 'Juan';
        personB['batch'] = 89;

    //with final, once value has set, it cannot be changed.
    final DateTime now;
    now = DateTime.now();

    //with const, an identifier must have a corresponding declaration of value
    const String companyAcronym = 'FFUF';

    print(firstName + ' ' + lastName);
    //print('$firstName $lastName'); 
    print('Full Name: $firstName $lastName');
    print('Age: ' + age.toString());
    print('Height: '+ height.toString());
    print('Weight: ' + weight.toString());
    print('Registered: ' + isRegistered.toString());
    print('Grades: ' + grades.toString());
    print('Current Date Time: ' + now.toString());


    print(personB['name']);
}