import './freezed_models/user.dart';


void main() {
    User userA = User(id: 1, email: 'john@gmail.com');
    User userB = User(id: 1, email: 'john@gmail.com');


    print(userA.hashCode);
    print(userB.hashCode);
    print(userA == userB);


    //Demonstration of object immutability below
    //immutability means changes are not allowed
    //it ensures than an object will not be changed accidentally
    
    //instead of directly changing object's property
    //the object itself will be changed or reassigned with new values
    //to achieve this, we use the object.copyWith() method.

   /*  print(userA.email);
    userA = userA.copyWith(email: 'john@hotmail.com');
    print(userA.email); */

    var response = {'id': 3, 'email': 'doe@gmail.com'};

    /* User userC = User(
        id: response['id'] as int,
        email: response['email'] as String
    ); */

    User userC = User.fromJson(response);
    print(userC);
    print(userC.toJson());
}