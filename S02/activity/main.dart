

void main(){
   Map<String, dynamic> person ={
      'First Name': 'John', 
      'Last Name': 'Smith', 
      'Age': 34,  
    };

  Set<String> processors = {
    'Ryzen 9 5950X',
    'Ryzen 7 5800X',
    'Ryzen 3 3200G',
    'Ryzen 5 3600'
};

  List<String> country = [  
      'Thailand',
      'Canada',
      'Philippines',
      'Singapore',
      'United Kingdom',
      'Japan'
  ];

  print(person.length);
  print(processors.length);
  print(country.length);


}