

void main(){
    
    //Set<dataType> variableName = {values}

    //a set in dart, is an ordered collection of unique items
    Set<String> subContractors  = {'Sonderhoff','Stahlschmidt'};
    subContractors.add('Flula');
    subContractors.add('Kreatel');
    subContractors.add('Kunstoffe');
    subContractors.remove('Sonderhoff');

    print(subContractors); 
}