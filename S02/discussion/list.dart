

void main(){
  //Lis<tDataType> variableName = [values]
  List<int> discountRanges = [20, 40, 60, 80];
  List<String> names = //const can be declared before list word or after the indetifier/before bracket
    [
      'John',
      'Jane',
      'Tom'
  ];

  const List<String> maritalStatus = [
      'single',
      'married',
      'divorced',
      'widowed'
  ];

  print(discountRanges);
  print(names);
 
  print(discountRanges[2]);
  print(names[0]);

  print(discountRanges.length);
  print(names.length);

  names[0] = 'Jonathan';
  print(names);

  names.add('Mark'); //add element to the end of list
  names.insert(0,'Roselle');
  print(names);

  //properties
  print(names.isEmpty);
  print(names.isNotEmpty);
  print(names.first);
  print(names.last);
  print(names.reversed);

  //method
  //this method modifies the list itself
  names.sort();

  print(names.reversed);
}