

void main() {
 /*  print(getCompanyName());
  print(getYearEstablishment());
  print(hasOnlineClasses()); */
  // print(getCoordinates());
  // print(combinedAddress('134 Timog Ave', 'barangay Sacred Heart', 'Quezon City', 'Metro Manila'));
  // print(combineName('John','Smith'));
  // print(combineName('John','Smith', isLastNameFirst: true));
  // print(combineName('John','Smith', isLastNameFirst: false)); 

  print(isUnderAge(18));

  List<String> persons = ['John Doe', 'Jane Doe'];
  List<String> students = ['Nicholas Cage', 'James Bond'];

  //Anonymous function
 /*  persons.forEach((String person) {
      print(person);
   });

  students.forEach((String person) {
      print(person);
   }); */


  //function as objects AND used as an argument
  //printName(value) is function execution/call/invocation.
  //printName is reference to the given function
  persons.forEach(printName);

  students.forEach(printName);
}


void printName(String name){
    print(name);
}


//optional named parameters
//these are parameters added after required ones. 
//these parameters are added inside a curly bracket
//if no value is given, a default value can be assigned
String combineName(String firstName, String lastName, { bool isLastNameFirst  = false }){
    if(isLastNameFirst){
          return '$lastName $firstName';
    } else {
          return '$firstName $lastName';
    }
}


String combinedAddress(String specifics, String barangay, String city, String province){
    return '$specifics, $barangay, $city, $province';
}


String getCompanyName(){
  return 'FFUF';
}

int getYearEstablishment(){
  return 2021;
}

bool hasOnlineClasses(){
  return true;
}


// bool isUnderAge(int age){
//   return (age < 18) ? true : false;
// }

// the initial isUnderAge function can be changed into a LAMBDA (arrow) function
//a lambda function is a shortcut function for returning values from simple operations.
// the syntax of a lambda function is:
// return-type function-name(parameters) => expression;
bool isUnderAge(int age) => (age < 18) ? true :  false;


Map <String, double> getCoordinates(){
  return {
      'latitude': 14.5423,
      'longitude': 15.2345
  };
}

// the following syntax is followed when creating a function
/*
      return type function-name(param-data-type parameterA, param-data-type parameterB){
        //code logic inside a function
        return 'return value';
      }

 */

//arguments are the values being passed to the function
//parameters are the values being received by the function


//x (parameter) = 5 (argument)

//in Dart, writing the data type of a function