//Solution to s04 activity
void main() {
    List<num> prices = [45, 34.2,176.9, 32.2];
    num totalPrice = getTotalPrice(prices);

    print(totalPrice);
    print(getDiscountedPrice(totalPrice, 0.2));
    print(getDiscountedPrice(totalPrice, 0.4));
    print(getDiscountedPrice(totalPrice, 0.6));
    print(getDiscountedPrice(totalPrice, 0.8));
}

num getTotalPrice(List<num> prices) {
    num totalPrice = 0;

    prices.forEach((num price) {
        totalPrice += price;
     });
     return totalPrice;
}

num getDiscountedPrice(num totalPrice, num percentage){
  return totalPrice = (totalPrice * percentage);
}