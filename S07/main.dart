
import './worker.dart';


void main() {
      Doctor doctor =  new Doctor(firstName: 'John', lastName: 'Smith');
      Carpenter carpenter = new Carpenter();

      print(carpenter.getType());
}

abstract class Person {

      //the class Person defines that getFullName must be implemented
      //however, it does not tell the concrete class HOW to implement it
      String getFullName();
}

//the concrete class id Doctor class
class Doctor implements Person{

      String firstName;
      String lastName;

      Doctor({
        required this.firstName,
        required this.lastName
      });


      String getFullName(){
          return 'Dr. ${this.firstName} ${this.lastName}';
      }
}