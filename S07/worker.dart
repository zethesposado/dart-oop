


abstract class _Worker{
      String getType(); //making this private, cant used getType outside the file
      
}

class Carpenter implements _Worker{
      String getType(){
          return 'Carpenter';
      }
}