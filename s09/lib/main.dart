import 'package:flutter/material.dart';
import './body_questions.dart';
import './body_answers.dart';


void main() {
    //the App() is the root widget
    runApp(App());  //new word can be omitted  
}

class App extends StatefulWidget {
    @override
    AppState createState() => AppState();
}

class AppState extends State<App> {
    int questionIdx = 0;
    /* List<String> questions = [
        'What is the nature of your business',
        'What is the expected size of user base?',
        'In which region would the majority of the user base be',
        'What is the expected project duration?'
    ]; */

    /*  List<List<String>> answers = [
        ['Time Tracking', 'Asset Management','Issue Tracking'],
        ['Less Than 1,000','Less Than 10,000','More Than 10,000' ],
        ['Asia','Europe', 'Americas','Africa','Middle East'],
        ['Less Than 3 months', '3-6 months', '6-9 months', '9-12 months', 'More Than 12 months']
    ];
    */

    bool showAnswers = false;

    final questions = [
        {
            'question': 'What is the nature of your business?',
            'options': ['Time Tracking', 'Asset Management','Issue Tracking']
        },
        {
            'question': 'What is the expected size of user base?',
            'options': ['Less Than 1,000','Less Than 10,000','More Than 10,000']
        },
        {
            'question': 'In which region would the majority of the user base be?',
            'options': ['Asia','Europe', 'Americas','Africa','Middle East']
        },
        {
            'question': 'What is the expected project duration?',
            'options': ['Less Than 3 months', '3-6 months', '6-9 months', '9-12 months', 'More Than 12 months']
        }
    ];

    var answers = [];

    void nextQuestion(String? answer){
        
        answers.add({
           'question': questions[questionIdx]['question'],
           'answer': (answer == null) ? '' : answer 
        });

        if(questionIdx < questions.length - 1){
            setState(() => questionIdx++);
        } else {
            setState(() => showAnswers = true);
        }
    }

    @override
    Widget build(BuildContext context) {   

        var bodyQuestions = BodyQuestions(questions: questions, questionIdx: questionIdx, nextQuestion: nextQuestion);
        var bodyAnswers = BodyAnswers(answers: answers);

        Scaffold homepage = Scaffold(
            appBar:  AppBar(title: Text('Homepage')),
            body: (showAnswers) ? bodyAnswers : bodyQuestions
        ); 

        return MaterialApp(
            home: homepage
        );
    }
}

    //double infinity is equiavalent of width = 100 in CSS.

    //responsible for grouping
        //layout widgets/
        //invisible widgets are container and scaffold
        //the visible widgtes are AppBar and Text. 

        //to specify margin or padding spacing, use edgeInsets, 
        //values for spacing are in multiples of 8 (eg. 16, 24, 32)
        //to add spacing on all sides, use EdgeInsets.all
        //to add spacing on certain sides, use EdgeInsets.only(direction: value) 
        //to put colors on a container, the decoration: BoxDecoration(color: color.value) can be used.

    //also came from material design
        //scaffold widget provides basic design layout structure
        //scaffold can be given UI elements such as an app bar

        //the state if lifted up
        //App.questionIdx = 1
        //->BodyQuestions.questionIdx = 1