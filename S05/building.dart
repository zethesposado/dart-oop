class Building { //late
     String? _name;
     int floors;
     String address;

    Building( this._name, {
     
      required this.floors, 
      required this.address
    })  {
        print('A building object has been created');
    }
    //the get and set allows for indirect access to class fields
    String? get Name {
      print('The building \'s name has been retrieved');
      return this._name;
    }

    void set Name(String? name){
      this._name = name;
      print('The building \'s name has been changed to $name');
    }

    Map <String, dynamic> getProperties(){
      return {
          'name': this._name,
          'floors': this.floors.toString(),
          'address': this.address
      };
    }

} 

//After addressing the nullable value
/* class Building { //late
     String name;
     int floors;
     String address;

    Building(this.name, this.floors, this.address){
        print('A building object has been created');
    }
} */

//before addressing the nullable fields
/* class Building { //late
     String? name;
     int? floors;
     String? address;

    Building(String name, int floors, String address){
        this.name = name;
        this.floors = floors;
        this.address = address;
    }
} */

// the class declaration looks like this:
/*
    class <ClassName>{
        <fields>
        <constructors>
        <methods>
        <getters-or-setters>
    }
 */